<!--#include virtual="/includes2013/SSI/config/global.config" -->

returnToList=0;
hashNavigation = {};

$(function(){ // document ready

    $('.credit').insertAfter('article');

    $(window).load( function(){

        $(document).trigger('scroll');
        $('body').addClass('loaded');
        $('.loading').animate({
            opacity: 0,
        }, 1000, function() {
            $('.loading').fadeOut(200);
        });
        $('.specialeCorriere').animate({
            opacity: 1,
        }, 666, function() {
        });

         var num = $(".donna-image img").length;
		   //console.log(num);
          $(".donna-image img").each(function() {  
		   imgsrc = this.src;
		   //console.log(imgsrc);
		  });
		//console.log(Math.floor((Math.random() * num) + 1));
      });

    // var $imgs = $(".photo > img");
    // var interval = setInterval(function () {
    //     var $gs = $imgs.not(".zind");
    //     $(".photo > img").removeClass('zind');
    //     $gs.eq(Math.floor(Math.random() * $gs.length)).addClass('zind');
    //     if ($gs.length == 1) {
    //         clearInterval(interval);
    //     }
    // }, 3000);
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = $('.cd-top');
    var descrizioni = [];
    var total_number = descrizioni.length-1;
    var position =  0;
    hashNavigation.start = position;
    posHtml = $(".contatore p");
    //$('.fitText').bigtext();
    var options = {
        valueNames: [
            'donna-titolo',
            { data: [
                    'ordine',
                    'nome',
                    'voce1',
                    'voce2',
                    'voce3',
                    'voce4',
                    'titolo'
                ] }
        ]
    };

    var donneLista = new List('listaDonne', options );
    var activeFilters = [];
    var dataLayer = window.dataLayer = window.dataLayer || [];
    dataLayer.push({
      'pagename': '12 donne, chi sarà la vice di Joe Biden'
    });

function showDescription(lista, index, totale, click){
    click = (typeof click !== 'undefined') ?  click : true;
    //console.log(lista, index, totale);
    $('.donneList li').removeClass('active');
    $('.donneList li[data-id="' + lista[index] + '"]').addClass('active');
     if(click){
        //window.location.hash = lista[index];
    }
    posHtml.html( "<strong>" + ( parseInt(index) + 1 ) + "</strong> di " + totale);
    position = index;   
}
// Filtri menu
    $('#filter_Continente').change(function() {
        var value = this.value;
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {         
            if (item.values().continente == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });
    $('#filter_Area').change(function() {
        var value = this.value;
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {
            if (item.values().area == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });
    $('#filter_Eta').change(function() {
        var value = this.value;  
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {      
            if (item.values().fascia_eta == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });
    $('#filter_Categoria').change(function() {
        var value = this.value;
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {
            if (item.values().categoria == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });
    $('#filter_Paese').on("click touch", function() {
        $(this).toggleClass("selected");
        if( $(this).hasClass("selected") ) {          
        $('.filtro select').not(this).prop('selectedIndex',0);
        // $('.filtro button').removeClass('selected').addClass('all');
        donneLista.filter(function (item) {
            if (item.values().paese == "Italia") {
                return true;
            } else {
                return false;
            }
        });    
        }
        else {
            donneLista.filter(); // Remove all filters
        }
        return false;
     });
/* Fine filtri */   
    donneLista.on('updated', function(elem){
        //console.log(elem.visibleItems);
        total_number =  elem.matchingItems.length;
        posHtml.html("0 di " + total_number);
        offset = $('#long_desc').offset().top - $(window).height();
        if( 0 == elem.matchingItems.length ){
            posHtml.html("nessun elemento");
            $('.contatore').css('display','none');
            $('.messages').html("<p class='notFound'>Spiacente, la ricerca non ha prodotto nessun risultato.</p>");
            descrizioni = [];
            position = 0;
        } else {
            $('.contatore').css('display','block');
            $('.messages').html("");
            descrizioni = [];
            position = 0;
            $.each(elem.matchingItems, function(k, v){
                descrizioni.push( v._values.id );
            });
        }
            showDescription(descrizioni, position, total_number);
    });
    donneLista.update();
    hashNavigation.total = donneLista.matchingItems.length;

    if( !(window.location.hash == null || window.location.hash == '' || window.location.hash == undefined) ){
        showDescription(descrizioni, window.location.hash.replace('#',''), hashNavigation.total);
        $("html, body").animate({ scrollTop: $('#long_desc').offset().top }, 1000);
        } else {
            showDescription(descrizioni, 0, total_number, false );
        }
    /*$('.contatore div').on('click', function(){
            if ( $(this).hasClass('prev') ){
                position--;
                if(position<0) { position = descrizioni.length-1; }
            } else if ( $(this).hasClass('next') ){
                position++;

                if(position>descrizioni.length-1) { position = 0; }
            }
            showDescription(descrizioni, position, total_number);
        });*/

/* Gesitione hover */ 
    $('#listaDonne ul').on("mouseleave touchmove", function(){
        // console.log("enter");
        $('.donna-image img').css('filter','saturate(100%)');
    })
    $('#listaDonne li').on("touchstart mouseenter", function(){
        // console.log("enter");
        $(this).addClass('tooltip');
        $('.donna-image img').css('filter','saturate(40%)');
        $('img',this).css('filter','saturate(100%)');
    }).on("mouseleave touchmove", function(){
        // console.log("leave");
        $(this).removeClass('tooltip');
    }).on('click', function(){
        // console.log("click");
        $(this).removeClass('tooltip');
        returnToList = $(this).offset().top;
        var current = $(this).attr('data-id');
        console.log(current);
        $("html, body").animate({ scrollTop: $('.descrizione[data-id="'+current+'"]').offset().top - 220 }, 1000);
        showDescription(descrizioni, descrizioni.indexOf($(this).attr("data-id")) , total_number);
    });

    $('ul.vocette li').on('click', function(){
        hide = $(this).attr('class');
        $("span.tutti").css('opacity','1');
        $(".list li").css('display','none');
        $(".list li."+hide).css('display','block');
    });

    $('span.tutti').on('click', function(){
        $("span.tutti").css('opacity','');
        $(".list li").css('display','');
    });

    $('a.linke').on('click', function(){
        // console.log("click");
        //$(this).removeClass('tooltip');
        returnToList = $(this).offset().top;
        $("html, body").animate({ scrollTop: $('#long_desc').offset().top }, 1000);
        showDescription(descrizioni, descrizioni.indexOf($(this).attr("data-id")) , total_number);
        });
    $('a.credits').on('click', function(){
        $("html, body").animate({ scrollTop: $('#crediti').offset().top }, 1000);
    });
    $('.scrollup').on('click', function(){
        $("html, body").animate({ scrollTop: 0 }, 1000);
    });
    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) { 
            $back_to_top.addClass('cd-fade-out');
        }
    });
    // fixed faccini
    //store the element
    $('.descrizione-image').each(function( index ) {
    var $cache2 = $(this);
    //store the initial position of the element
    var vTop = $cache2.offset().top -220;
      $(window).scroll(function (event) {
        // what the y position of the scroll is
        var y = $(this).scrollTop();
        // whether that's below the form
        if (y >= vTop) {
          // if so, ad the fixed class
          $cache2.addClass('fixed');
          //$cache2.parent().prev().find('.descrizione-image').removeClass('fixed');
        } 
        if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
           $('.descrizione-image').removeClass('fixed');
        }
        if (y < vTop) {
          // otherwise remove it
          $cache2.removeClass('fixed');
          //$cache2.parent().prev().find('.descrizione-image').addClass('fixed');
        }
      });
    });
    // fixed faccini
    //store the element
    var $cache = $('section.lista');
    //store the initial position of the element
    var vTop = $cache.offset().top;
      $(window).scroll(function (event) {
        // what the y position of the scroll is
        var y = $(this).scrollTop() +10;
        // whether that's below the form
        if (y >= vTop) {
          // if so, ad the fixed class
          $cache.addClass('fixed');
          $('.nonvisibili').addClass('visibili');
        } else {
          // otherwise remove it
          $cache.removeClass('fixed');
          $('.nonvisibili').removeClass('visibili');
        }
      });
    //smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0 ,
            }, scroll_top_duration
        );
    });
        /*$('.descrizione').swipe( {
            swipeLeft:function(event, direction, distance, duration, fingerCount) {
                console.log(event, direction, distance, duration, fingerCount);
                position++;
                if(position>descrizioni.length-1) { position = 0; }
                showDescription(descrizioni, position, total_number);
            },
            swipeRight:function(event, direction, distance, duration, fingerCount) {
                position--;
                if(position<0) { position = descrizioni.length-1; }
                showDescription(descrizioni, position, total_number);
            },
            //Default is 75px, set to 0 for demo so any distance triggers swipe
            threshold:100,
            excludedElements:$.fn.swipe.defaults.excludedElements+", .social_donna"
        });*/
$('.random').on('click touch',function(){
    //console.log(lista, index, totale);
    window.location.hash = Math.floor( Math.random() * 100 );
    window.location.reload(true);
});

/* SOCIAL */
    $('.fa-facebook-official').on("click touch", function() { 
        //var campo = $(this).parent().parent().prev().text(); 
        var tit = $(this).attr('data-nome');
        var tit2 = $(this).attr('data-cognome');
        var campo = $(this).attr('data-nome') +" "+ $(this).attr('data-cognome') + " la vice di Biden?";
        if(!tit){
          campo ="Elezioni Usa 2020, chi la vice di Biden?";
        }
        console.log(campo);
        window.open('https://www.facebook.com/sharer/sharer.php?app_id=203568503078644' + 
        '&u=https://www.corriere.it/speciale/esteri/2020/elezioni-usa-2020-vice-biden/index.shtml' + 
        '&quote=' + encodeURIComponent( campo ) +
        '&display=popup', 'sharer', 'width=626,height=436');
    }); 
  $('.fa-twitter-square').on("click touch", function() { 
        var tit = $(this).attr('data-nome');
        var tit2 = $(this).attr('data-cognome');
        var campo = $(this).attr('data-nome') +" "+ $(this).attr('data-cognome') + " la vice di Biden?";
        if(!tit){
          campo ="Elezioni Usa 2020, chi la vice di Biden?";
        }
        window.open('https://twitter.com/share?' + 
        '&url=https://www.corriere.it/speciale/esteri/2020/elezioni-usa-2020-vice-biden/index.shtml' +  
        '&text=' + encodeURIComponent( campo ) +  
        '&related=corriere_it', 'sharer', 'width=626,height=436');       
    }); 
});







